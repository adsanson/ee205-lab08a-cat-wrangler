///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11/4/2021
///////////////////////////////////////////////////////////////////////////////
#include "node.hpp"
#include "list.hpp"

#pragma once

class Queue {
protected:
	Doublelinkedlist list = Doublelinkedlist();

public:
	inline const bool empty() const { return list.empty(); };
   inline const unsigned int size() const {return list.size();};
   inline const void push_front( Node* newNode ) const {return push_front( newNode );};
   inline const Node* pop_back() const {return pop_back();};
   inline  Node* get_first() const {return get_first();};
   inline  Node* get_next(const Node* currentNode) const {return get_next(currentNode);};
}; // class Queue
