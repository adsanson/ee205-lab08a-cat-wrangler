///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   12_04_2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "list.hpp"

const bool Doublelinkedlist::empty() const{
   return head == NULL;
}

void Doublelinkedlist::push_front (Node* newNode){
   if(Doublelinkedlist::empty()){
   head=newNode;
   tail=head;
   head->next=tail;
   tail->prev=head;
   }
   else if(head == tail){
      newNode->next=tail;
      head=newNode;
      tail->prev=head;
   }
   else{
      newNode->next=head;
      head->prev=newNode;
      head=newNode;
   }

}

void Doublelinkedlist::push_back (Node* newNode){
   if(Doublelinkedlist::empty()){
   tail=newNode;
   head=tail;
   tail->prev=head;
   head->next=tail;
   }
   else if(head == tail){
      newNode->prev=tail;
      tail=newNode;
      head->next=tail;
   }
   else{
      newNode->prev=tail;
      tail->next=newNode;
      tail=newNode;
   }

}

Node* Doublelinkedlist::pop_front(){
   if(!Doublelinkedlist::empty() ){
      Temp = head;
      head=head->next;
   }
   return Temp;
}

Node* Doublelinkedlist::pop_back(){
   if(!Doublelinkedlist::empty() ){
      Temp = tail;
      tail=tail->prev;
   }
   return Temp;
}

Node* Doublelinkedlist::get_first() const {
   return head;
}

Node* Doublelinkedlist::get_last() const {
   return tail;
}

Node* Doublelinkedlist::get_next( const Node* currentNode ) const{
   if(currentNode != tail)
   return currentNode->next;
   else return nullptr;
   
}

Node* Doublelinkedlist::get_prev (const Node* currentNode) const{
   if (currentNode != head)
   return currentNode->prev;
   else return nullptr;
}

unsigned int Doublelinkedlist::size() const{
   int i=0;
   Node* temp = head;
   while(temp != nullptr){
      temp = temp->next;
      i++;
   }
   return i;
}
