///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.hpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11/4/2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

class Node {
friend class Doublelinkedlist;

protected:
	Node* next = nullptr;
   Node* prev = nullptr;

public:
	virtual bool operator>(const Node& r);

}; // class Node
