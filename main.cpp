///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11/4/2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"
#include "list.hpp"

using namespace std;

// The purpose of this main() function is to exercise your
// DoubleLinkedList and quickly visualize the correct operation
// of the methods.

int main() {
   cout << "Welcome to Cat Wrangler" << endl;

	Cat::initNames();

	Cat* newCat = Cat::makeCat();

	cout << newCat->getInfo() << endl;
   cout << "========" << endl;

	Doublelinkedlist list = Doublelinkedlist();

	// Put 16 cats in my list
	for( int i = 0 ; i < 8 ; i++ ) {
		list.push_front( Cat::makeCat() );
		list.push_back( Cat::makeCat() );
	}

//	list.insertionSort();  // They should be sorted by name

	// Swap the first 2 and the last 2 cats (using 2 different techniques)
	Cat* cat1 = (Cat*)list.pop_front();
	Cat* cat2 = (Cat*)list.pop_back();

//	list.swap( list.get_first(), list.get_last() );

	list.push_back( cat1 );
	list.push_front( cat2 );

   for( Cat* cat = (Cat*)list.get_last() ; cat != nullptr ; cat = (Cat*)list.get_prev( cat )) {
		cout << cat->getInfo() << endl;
	}
}
