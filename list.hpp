///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file list.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   12_04_2021
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include "node.hpp"
#include<cassert>
#pragma once

class Doublelinkedlist{
   protected:
   Node* head = nullptr;
   Node* tail = nullptr;
   Node* Temp = nullptr;

   public:
   const bool empty() const;
   void push_front( Node* newNode );
   Node* pop_front();
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
   void push_back( Node* newNode );
   Node* pop_back();
   Node* get_last() const;
   Node *get_prev( const Node* currentNode ) const;
   void insert_after(Node* currentNode, Node* newNode);
   void insert_before(Node* currentNode, Node* newNode);
   void swap(Node* node1, Node* node2);
   const bool isSorted() const;
   void insertionSort();
   bool validate() const;
   void dump() const;
   unsigned int size() const;
   const bool isIn(Node* aNode) const;
};
