///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file node.cpp
/// @version 1.0
///
/// Exports data about all cats
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   11/4/2021
///////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <iomanip>

#include "node.hpp"

using namespace std;


// This sorts based on the Node's address (because Node doesn't hold any
// data).  However, classes like Cat can override this operator and implement
// their own lexigraphic test for sorting.

bool Node::operator>(const Node& rightSide) {
	// this is the leftSide of the operator, so compare:
	// leftSide > rightSide

	if( this > &rightSide )
		return true;
	return false;
}

